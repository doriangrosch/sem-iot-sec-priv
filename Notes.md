[1] done
[2] done / 6.0 skipped weil nur Beispiele
[3] done
[4] done
[5] done
[9] done
[10] security section nicht gemacht
[11] done
[12] fehlt Kapitel 5 bis ---
[13] done
[14] skip
[15] skip
[16] 
[17] ? (skip)
[18] skip
[19] halb
[20] done
[21]
[22]
[23]

# Paper / Drafts

* Requirements and Challenges for IoT over ICN
        https://datatracker.ietf.org/doc/draft-zhang-icnrg-icniot-requirements/
* ​Proposed Design Choices for IoT over Information Centric Networking
        https://datatracker.ietf.org/doc/draft-lindgren-icnrg-designchoices/
* Design Considerations for Applying ICN to IoT
        https://datatracker.ietf.org/doc/draft-irtf-icnrg-icniot/

# RFCs

* RFC 7476 Information-Centric Networking: Baseline Scenarios
        http://tools.ietf.org/html/rfc7476
* Information-Centric Networking (ICN) Research Challenges
        http://tools.ietf.org/html/rfc7927
* Information-Centric Networking: Evaluation and Security Considerations
        http://tools.ietf.org/html/rfc7945

# All docs & RFCs:

* Information-Centric Networking (icnrg) 
        https://datatracker.ietf.org/group/icnrg/documents/

# Named Data Networking

* Named Data Networking (NDN) Project
        http://named-data.net/techreport/TR001ndn-proj.pdf
* Natural Object Security
