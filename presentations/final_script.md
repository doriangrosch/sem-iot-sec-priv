---
papersize: a5
geometry: margin=2cm
fontsize: 12pt
---

## Introduction and Motivation

How we use the Internet is changing. While the current Internet was originally about long range, point-to-point communication, nowadays the focus is mainly on delivering data, usually media content, through the network to a large amount of consumers. 

Since our current Internet architecture was not originally designed for this, it could be profitable to develop a new architecture that is more suited to the task. Researchers in this field are confronted with several challenges. 

First of all, a new Internet architecture should be scalable, in terms of number of connected devices and traffic load. Also, a lot of devices are going to be mobile, for example smartphones, and are going to move around the physical world and roam through different networks while being online. Security will be a major concern. The security mechanisms of the current Internet were developed in time and are more of an Add-On. What we would like from future architectures would be a Security-by-Design approach.

A new Internet architecture which is a active research subject is Information Centric Networking. It could very well tackle these challenges in the future.

\newpage

## Information Centric Networking

### Central aspects

There are several central aspects to the paradigm of Information Centric Networking.

First of all, data will be named. This means that every single data object will receive a unique name that identifies it, and can even provide security functionality on top. For example, names usually contain the hash of the data objects, so you can easily authenticate received data.

Second, ICN decouples information from its location. That means that data will not only be found at its source, but it is indipendent from where its actually stored. Data is more of an entity in the network.

This abstract view of data means that caching is possible at the network level. Data objects can be temporarily stored in devices all across the network. By bringing the content closer to the consumers caching can lead to less network load and faster content delivery.

Data transfers are initialised when content is explicitly requested by a consumer. The request is sent to the network itself, not to a specific host. The consumer asks for a certain name -- if the corresponding data object is found in the network, it is delivered to the consumer.

Finally, a big part of the security of ICN is that individual data objects are encrypted. In this way, we don't need a secure communication channel, since the content is sent over the network in an encrypted state.

These are the central properties that characterize ICN. 

\newpage

### Requirements and Challenges

While designing ICN, there are certain requirements, both technical and architectural, that need consideration. I would like to present some of them here.

Devices connected to ICN are expected to be very different from another, ranging from low-end embedded devices to high-end servers. Also, the means of connecting to the network will be very hetergeneous.

Also, there will be a huge number of connected devices, especially when considering the emerging Internet of Things. Many of these will be mobile.

The naming scheme and data delivery mechanism will have to be fast and efficient to make ICN a real alternative to the current Internet.

Furthermore, there is a need for using location information and to take advantage of the multicast and broadcast mechanisms, which are built-in into ICN.

As I already said, security and privacy are a fundamental requirement for modern network design. Security requirements for ICN are trust management, data integrity, authentication and access control. Privacy in ICN is about encrypting data objects and hiding or preventing metadata as far as possible.

Of course there are many challenges that engineers have to face in building a new Internet architecture like ICN. Just for example, a challenge would be aiming for high security and privacy, all the while having to deal with resource constrained low-end devices.

\newpage

### Design Choices

There are many ways to design an ICN. The choices made in these areas can have great impact on the design and functionality of ICN deployments.

In my paper, I go into detail about these design choices. However, to name them all would exceed the scope of this presentation. But I want to give some examples:

The naming scheme could use hierarchical names or flat names. The name resolution can be coupled to data routing or be indipendent from it. Caching can be designed to be implemented on-path or off-path. The degree of decoupling between network components in ICN has to be decided, and one has to choose what kinds of security mechanisms are to be implemented in practice.

\newpage

## IoT and ICN

### Specific aspects and problems

Up next, I want to talk about how Information Centric Networking can be applied to the Internet of Things.

First of all, the Internet of Things as a new network paradigm has some specific aspects to it. The IoT is made of a huge number of embedded devices which are able to network and act as sensors and actuators. They are going to be very resource-constrained in terms of computing capability and battery power. The caching functionality of ICN can help with saving power, so that the devices don't have to wake up every time some content is requested.

Many IoT applications are about aggregating lots of data from sensors. From this, one could ea	sily see why ICN seems to fit well as an underlying network technology. With ICN, several other problems like limited IP space and efficient transport of data can be solved. Also, the ICN network would distribute content and provide a good addressing scheme at the same time.

However, there is a problem. If ICN is about consumers requesting data from the network, how can actuators in specific IoT devices be controlled at all? One possible solution is that an IoT device itself could become a consumer and request its current actuator state from the network.

Another issue is with the naming scheme. Often, IoT data is very small, usually just a sensor reading that fits into 32 bits. The names of these small data objects, however, could get very long. So developers have to be alert to this possible naming overhead and design the system accordingly.

\newpage

Another challenge is getting the newest data from the network. If every data object published by an IoT device has a unique name, how can a consumer just get the newest data from the network? A possible solution could be reading it out from meta-data that the consumer could receive from the network.

Also, an IoT network may generate a lot of data in short periods of time. ICN was not originally designed for this. Still, a good trade-off between a host-centric and information-centric approach could be found. For example, we could have ICN do naming and addressing, while the Internet Protocol could be used for routing.

\newpage

## A secure ICN-IoT architecture

In the next section, I will talk about a proposed ICN architecture for the Internet of Things. In my paper, I summarize the work of Sicari et al.. The authors analyze the current security functionalities of the ICN-IoT middleware proposed by the ICNRG, the Information Centric Networking Research Group.

First of all, I want to give an overview of the proposed architecture. This middleware introduces a set of network entities, tools and interfaces to build a standardized platform for IoT systems.

The system needs to be able to satisfy three important goals: providing a trust model for the involved entities and relationships, preserving the privacy for sensitive information transmitted within the network and guaranteeing an effective access control system which is based on well-defined, cross-domain policies.

The ICN-IoT architecture has five main components. The embedded systems are the IoT devices that sense and act on the real world. The Aggregator is a kind of local network gateway that acts as a bridge between the IoT devices and the network. It collects data from the devices and can perform basic services. 

The Local Service Gateway connects this local ICN system to the global one and handles name assignment, for example. The ICN-IoT Server manages the lookup services and content subscriptions from consumers. And finally, the Service Consumers are the applications or users who want to receive data or use other services provided by the IoT network.

\newpage

### Example

To give you an example, I would like to show you how the ICN-IoT middleware would handle secure device discovery. The goal of device discovery is establishing secure relationships between devices, in this case between a device and the Aggregator.

The situation is as follows: a new device is powered on in the network. It is preloaded by the manufacturer with three credentials: an ID, a public / private key pair and a certificate which binds the ID and public key together.

The device advertises itself to the Aggregator and sends over its ID and certificate, and also a signature created with its private key. With this information, the Aggregator can verify the device identity. It stores the credentials and assigns an action key to the device. This action key is a symmetric key and it will be used for encrypting data objects in the future.

\newpage

## Conclusion

To conclude my presentation, I would like to touch on the main points.

ICN is a new Internet architecture that tries to fix some of the flaws of the current Internet and is more suited to the demands of future network. Since IoTs embody information-centric usage patterns, it can be profitable to apply ICNs as an underlying network infrastructure. With ICN-IoT, we saw a proposed IoT platform which emphasizes on security aspects to meet our high requirements.

Thank you for your attention.
